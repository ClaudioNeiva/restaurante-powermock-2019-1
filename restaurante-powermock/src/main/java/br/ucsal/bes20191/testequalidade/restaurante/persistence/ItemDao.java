package br.ucsal.bes20191.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20191.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class ItemDao {

	private static final String ITEM_NAO_ENCONTRADO = "Item não encontrado (código = %d).";
	
	public static List<Item> itens = new ArrayList<>();

	public static void incluir(Item item) {
		itens.add(item);
	}

	public static Item obterPorCodigo(Integer codigo) throws RegistroNaoEncontrado {
		for (Item item : itens) {
			if (item.getCodigo().equals(codigo)) {
				return item;
			}
		}
		throw new RegistroNaoEncontrado(String.format(ITEM_NAO_ENCONTRADO, codigo));
	}

}
